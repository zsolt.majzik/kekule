# Introduction

## Graph Convolutional Network (GCN) Layer (GCNConv) [1]:

Given a graph $\mathcal{G} = (\mathcal{V}, \mathcal{E})$, let $\tilde{A}$ be its adjacency matrix (with $\tilde{A}&#95;{ij} = 1$ if there is an edge between nodes $i$ and $j$, and $\tilde{A}&#95;{ij} = 0$ otherwise) and $\tilde{D}$ be the degree matrix (which is a diagonal matrix) where $\tilde{D}&#95;{ii} = \sum&#95;j \tilde{A}&#95;{ij}$ (sum of the weights of all edges connected to node $i$). Let $H$ be the feature matrix for the input layer, where each row corresponds to a node's features. The layer-wise propagation of a GCN can be defined as follows:

For each layer $l$, the feature representations $H^{(l)}$ are updated based on the previous layer's features $H&#94;{(l-1)}$ using the formula:

$$
H^{(l)} = \sigma\left(\tilde{D}^{-\frac{1}{2}} \tilde{A} \tilde{D}^{-\frac{1}{2}} H^{(l-1)} W^{(l-1)}\right)
$$

where, $\tilde{A} = A + I&#95;N$ is the adjacency matrix of the graph with added self-connections ($I&#95;N$ is the identity matrix), $W&#94;{(l-1)}$ is the weight matrix for layer $l-1$, and $\sigma$ denotes an activation function such as the ReLU function.

The output of the final layer $H^{(L)}$, after $L$ layers of propagation, is used for downstream tasks like node classification or graph classification.

## Graph Attention Network (GAT) Layer (GATConv) [2]:

The GAT layer, introduced by Petar Veličković et al., extends the idea of GCNs by incorporating attention mechanisms. The GAT layer computes the hidden representations by considering the weights between different nodes, which are learned during the training process.

For each layer, the forward pass update rule in a GAT is:

$$
H^{(l+1)}_i = \sigma \left( \sum_{j \in \mathcal{N}(i) \cup \{ i \} } \alpha_{ij}^{(l)} W^{(l)} H^{(l)}_j \right)
$$

where:

- $H^{(l)}_i$ is the feature vector of node $i$ at layer $l$
- $\mathcal{N}(i)$ denotes the set of neighbors of node $i$
- $\alpha_{ij}^{(l)}$ are the attention coefficients that are learned and indicate the importance of node $j$'s features to node $i$
- $W^{(l)}$ is the weight matrix for layer $l$
- $\sigma$ is a non-linear activation function.

The attention coefficients $\alpha_{ij}^{(l)}$ are computed as:

$$
\alpha_{ij}^{(l)} = \frac{
\exp \left( \text{LeakyReLU} \left( \mathbf{a}^{\top} \left[ \mathbf{W}^{(l)} \mathbf{H}_i^{(l)} \parallel \mathbf{W}^{(l)} \mathbf{H}_j^{(l)} \right] \right) \right)
}{
\sum_{k \in \mathcal{N}(i) \cup \{ i \}}
\exp \left( \text{LeakyReLU} \left( \mathbf{a}^{\top} \left[ \mathbf{W}^{(l)} \mathbf{H}_i^{(l)} \parallel \mathbf{W}^{(l)} \mathbf{H}_k^{(l)} \right] \right) \right)
}
$$

Here:
- $\mathbf{a}$ is a weight vector that is learned
- $\parallel$ denotes concatenation
- The numerator computes a normalized attention score between nodes $i$ and $j$
- The denominator is a normalization term that sums over all choices of node $j$ in the neighborhood of $i$, including $i$ itself.

GAT can potentially outperform GCN on tasks where the relationship or influence between nodes varies significantly and should be learned rather than assumed to be equal.

# Classification with GNN (MUTAG)


The MUTAG dataset is widely used in the field of graph machine learning, which is a subset of machine learning where the data is modeled as a graph. The dataset contains 188 graphs with 18 nodes and 20 edges on average for each graph. The graph nodes have 7 different labels/atom types, and the binary graph labels represent their mutagenic effect on a specific gram negative bacterium
The dataset is part of a large collection of different graph classification datasets, known as the TUDatasets, which is directly accessible via torch_geometric.datasets.

Mutagenicity corresponds to the ability of a substance to cause mutations. A mutation is a change in the DNA sequence within a cell. Mutagens are agents that can increase the frequency of mutation in an organism. In a medical or biological context, a substance that is mutagenic may be of concern because mutations can lead to diseases such as cancer.
Gram-negative bacteria are a group of bacteria that are characterized by their cell wall composition and are distinguished by the Gram stain test. Some compounds may have a mutagenic effect on these bacteria, meaning they can cause genetic mutations within the bacterial cells.

Based on the reference [4]

# Literature and references

[1]: Thomas N. Kipf and Max Welling: Semi-Supervised Classification with Graph Convolutional Networks (2017)

[2]: Petar Veličković et al.: Graph Attention Networks (2017)

[3]: Christopher Morris et al: Weisfeiler and Leman Go Neural: Higher-order Graph Neural Networks (2018)

[4]: [UVA Deep Learning Course Notes](https://uvadlc-notebooks.readthedocs.io/en/latest/index.html)