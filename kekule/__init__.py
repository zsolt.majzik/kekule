"""Kekule: the molecule former."""

__author__ = 'Zsolt Majzik <zsolt.majzik@gmail.com>'
__version__ = '0.1'
__version_info__ = tuple([int(num) for num in __version__.split('.')])
