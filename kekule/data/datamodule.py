import pytorch_lightning as pl
from torch.utils.data import random_split
from torch_geometric.datasets import TUDataset
from torch_geometric.loader import DataLoader


class MUTAGDataModule(pl.LightningDataModule):
    def __init__(self, dataset_path: str, val_split: float=0.2, batch_size: int = 32, num_workers: int = 4, pin_memory: bool = False):
        super().__init__()
        self.dataset_path = dataset_path
        self.val_split = val_split
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.pin_memory = pin_memory

        self.dataset = None
        self.train_dataset = None
        self.val_dataset = None
        self.test_dataset = None
        
        self.dataset = TUDataset(self.dataset_path, name='MUTAG')

    def prepare_data(self):
        # Download the dataset
        TUDataset(self.dataset_path, name='MUTAG')
        
    @property
    def num_node_features(self):
        return self.dataset.num_node_features
    
    @property
    def num_classes(self):
        return self.dataset.num_classes

    def setup(self, stage: str | None = None):
        # Assign train/val datasets for use in dataloaders
          
        if stage == 'fit' or stage is None:
            # Calculate the number of samples to include in each set
            val_size = int(len(self.dataset) * self.val_split)
            train_size = len(self.dataset) - val_size

            # Split the dataset
            self.train_dataset, self.val_dataset = random_split(self.dataset, [train_size, val_size])

        # This is a placeholder test only
        if stage == 'test' or stage is None:
            self.test_dataset = self.dataset

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
        )

    def test_dataloader(self):
        return DataLoader(
            self.test_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
        )
