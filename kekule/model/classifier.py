import pytorch_lightning as pl
import torch
import torch.nn as nn
from torch import optim

from .gnn import GraphGNNModel


class GraphLevelGNN(pl.LightningModule):
    """Graph Level GNN model that wraps the GraphGNNModel for graph classification tasks.

    Args:
        model_kwargs: Keyword arguments for the GraphGNNModel.
    """

    def __init__(self, **model_kwargs):
        super().__init__()
        # Saving hyperparameters
        self.save_hyperparameters()

        # Initialize the GNN model
        self.model = GraphGNNModel(**model_kwargs)

        # Choose the appropriate loss function
        self.loss_module = nn.BCEWithLogitsLoss() if self.hparams.c_out == 1 else nn.CrossEntropyLoss()

    def forward(self, data: torch.Tensor, mode: str = 'train'):
        """Forward pass for the model. Computes the predictions and loss for the given data.

        Args:
            data: A batch of data containing the features, edge information, and batch indices.
            mode: The mode of operation ('train', 'val', or 'test').

        Returns:
            tuple: A tuple containing the loss and accuracy as tensors.
        """
        x, edge_index, batch_idx = data.x, data.edge_index, data.batch
        x = self.model(x, edge_index, batch_idx)
        x = x.squeeze(dim=-1)

        # Handle binary and multi-class cases
        if self.hparams.c_out == 1:
            preds = (x > 0).float()
            data.y = data.y.float()
        else:
            preds = x.argmax(dim=-1)

        # Compute loss and accuracy
        loss = self.loss_module(x, data.y)
        acc = (preds == data.y).sum().float() / preds.shape[0]
        return loss, acc

    def configure_optimizers(self):
        """Sets up the optimizer for the model.

        Returns:
            optimizer: The optimizer for the model.
        """
        # AdamW applies weight decay directly to the weights themselves after
        # the optimization step has been computed. AdamW can be better for
        # GNN training with sparse datasets (not proved here).
        optimizer = optim.AdamW(self.parameters(), lr=1e-2, weight_decay=0.0)
        return optimizer

    def training_step(self, batch: torch.Tensor, batch_idx: int):
        """Conducts a single training step.

        Args:
            batch: The data batch to train on.
            batch_idx: The index of the batch.

        Returns:
            loss: The loss tensor after the training step.
        """
        loss, acc = self.forward(batch, mode='train')
        self.log('train_loss', loss)
        self.log('train_acc', acc)
        return loss

    def validation_step(self, batch, batch_idx):
        """Conducts a single validation step.

        Args:
            batch (Data): The data batch to validate on.
            batch_idx (int): The index of the batch.
        """
        _, acc = self.forward(batch, mode='val')
        self.log('val_acc', acc, on_epoch=True, prog_bar=True, logger=True)

    def test_step(self, batch, batch_idx):
        """Conducts a single test step.

        Args:
            batch (Data): The data batch to test on.
            batch_idx (int): The index of the batch.
        """
        _, acc = self.forward(batch, mode='test')
        self.log('test_acc', acc)
