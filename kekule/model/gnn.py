import torch.nn as nn
import torch.nn.functional as F
import torch_geometric.nn as geom_nn
from torch_geometric.nn import MessagePassing, global_mean_pool

"""
GNNLayers layer types:
    GCN - Graph Convolutional Network (GCN) Layer (GCNConv):
        This layer implements the graph convolution operation as described
        by Kipf and Welling ("Semi-Supervised Classification with Graph 
        Convolutional Networks"). It is based on the principle of spectral
        graph convolutions, aiming to generalize the traditional convolutional
        neural network (CNN) operation from grid data to graph data.
        The GCNConv layer aggregates node features from neighbors, weighted 
        by the normalized graph Laplacian, to produce new node features that
        incorporate local graph structure.
    GAT - Graph Attention Network (GAT) Layer (GATConv):
        The GATConv layer incorporates a self-attention strategy. Instead of
        averaging neighbor features (GCN), GATs compute a set of attention
        coefficients for each neighbor, allowing the model to weigh neighbors
        differently when aggregating features. 
        The attention mechanism enables the model to learn dynamic weights for
        each neighbor, which is particularly beneficial in graphs where the
        importance of neighbors is not uniform and should be learned from
        the data. 
        Multi-head attention is used to further stabilize the learning process
        by averaging over multiple independent attention mechanisms.
        Based on Petar Velickovic et al. (Graph Attention Networks).
    GraphConv - Graph Convolution Layer (GraphConv):
        This layer is a simple and efficient framework for node feature
        learning on graphs. It applies a convolution-like operation that
        updates node representations by aggregating feature information from
        neighbors using a simple weighted sum, followed by an optional
        non-linearity. The GraphConv layer is versatile and can be used in
        various GNN architectures, providing a straightforward way to
        encapsulate both the graph structure and node features into the
        learning process.
        Based on Weisfeiler and Leman Go Neural ("Higher-order Graph
        Neural Networks").
        
    For additional layers see also:
    https://pytorch-geometric.readthedocs.io/en/latest/modules/nn.html
"""
GNNLayers = {
    'GCN': geom_nn.GCNConv,
    'GAT': geom_nn.GATConv,
    'GATv2': geom_nn.GATv2Conv,
    'GraphConv': geom_nn.GraphConv,
}


class GNNModel(nn.Module):
    def __init__(
        self,
        c_in: int,
        c_hidden: int,
        c_out: int,
        num_layers: int = 2,
        layer_type: str = 'GCN',
        dp_rate: float = 0.1,
        **kwargs,
    ) -> None:
        super().__init__()
        """
        Initialize the Graph Neural Network (GNN) model with specified parameters.

        Args:
            c_in: The dimensionality of the input features for each node.
            c_hidden: The size of the hidden layers in the GNN. This is the number of features
                            each node will have after passing through a hidden layer.
            c_out: The number of output features
            num_layers: The number of graph convolutional layers in the GNN. Defaults to 2.
            layer_name: The type of graph convolutional layer to use. This is typically a string
                                        that maps to a specific layer class implementation. Defaults to "GCN".
            dp_rate: The dropout rate applied after each graph convolutional layer to prevent
                                    overfitting. Defaults to 0.1.
            **kwargs: Arbitrary keyword arguments that can be passed to the graph convolutional layer classes.
        """
        gnn_layer = GNNLayers[layer_type]

        self.layers = nn.ModuleList()
        in_channels = c_in
        for _ in range(num_layers - 1):
            self.layers.append(gnn_layer(in_channels=in_channels, out_channels=c_hidden, **kwargs))
            self.layers.append(nn.ReLU(inplace=True))
            self.layers.append(nn.Dropout(dp_rate))
            in_channels = c_hidden
        self.layers.append(gnn_layer(in_channels=in_channels, out_channels=c_out, **kwargs))

        self.global_pooling = global_mean_pool
        self.classifier = nn.Linear(c_out, c_out)

    def forward(self, x, edge_index, batch_idx=None):
        """Forward pass of the GNNModel.

        It takes node features and graph structure
        as input and processes the information through the GNN layers.
        If a batch index is provided, it can be used for batching node features
        together, allowing the model to process multiple graphs simultaneously.

        Args:
            x: The input node features of shape (num_nodes, c_in), where num_nodes
                is the number of nodes in the graph, and c_in is the number of
                input features.
            edge_index: The edge indices representing the graph's connectivity in
                COO format (2, num_edges), where the first row holds the source
                node indices, and the second row holds the destination
                node indices.
            batch_idx: The batch index for each node of shape (num_nodes,),
                indicating which graph the node belongs to in a batch of graphs.
                Defaults to None, which implies that all nodes are from the same
                graph.

        Returns:
            Tensor: The output node features of the final GNN layer,
                of shape (num_nodes, c_out).
        """
        for layer in self.layers:
            if isinstance(layer, MessagePassing):
                x = layer(x, edge_index)
            else:
                x = layer(x)

        # If batch_idx is provided, apply global mean pooling
        if batch_idx is not None:
            x = self.global_pooling(x, batch_idx)

        # Apply the classifier
        x = self.classifier(x)

        return F.log_softmax(x, dim=1)


class GraphGNNModel(nn.Module):
    def __init__(self, c_in, c_hidden, c_out, dp_rate_linear=0.5, **kwargs):
        """Initialize the GraphGNNModel.

        This combines a GNNModel for feature extraction
        with a final linear (output_layer) layer for
        graph-level output.

        Args:
            c_in: Dimension of input features.
            c_hidden: Dimension of hidden features within GNN layers.
            c_out: Dimension of output features, typically the number of classes.
            dp_rate_linear: Dropout rate before the final linear layer.
            **kwargs: Additional arguments passed to the GNNModel.
        """
        super().__init__()
        # Initialize the GNNModel with the same dimensions for input, hidden, and output.
        self.GNN = GNNModel(c_in=c_in, c_hidden=c_hidden, c_out=c_hidden, **kwargs)

        # Initialize the output_layer layer which consists of a dropout followed by a linear layer.
        # The linear layer transforms the features from the GNNModel to the final output size.
        self.output_layer = nn.Sequential(nn.Dropout(dp_rate_linear), nn.Linear(c_hidden, c_out))

    def forward(self, x, edge_index, batch_idx):
        """Forward pass of the GraphGNNModel.

        Args:
            x: Input features per node.
            edge_index: Edge index in COO format (source, target).
            batch_idx: Index of the batch for each node.
        """
        # Obtain node-level embeddings from the GNNModel.
        x = self.GNN(x, edge_index)
        # Perform global mean pooling to get graph-level representation.
        x = geom_nn.global_mean_pool(x, batch_idx)
        # Pass the pooled graph representation through the output_layer
        # to get the final shape.
        x = self.output_layer(x)
        return x
