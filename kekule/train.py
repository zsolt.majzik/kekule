import os

import hydra
from hydra.utils import to_absolute_path
from omegaconf import DictConfig
from pytorch_lightning import Trainer, callbacks, seed_everything
from pytorch_lightning.loggers import MLFlowLogger

from kekule.data.datamodule import MUTAGDataModule
from kekule.model.classifier import GraphLevelGNN


@hydra.main(config_path='conf', config_name='config')
def train_graph_classifier(cfg: DictConfig):
    seed_everything(42)

    dataset_path = to_absolute_path(cfg.paths.dataset_path)
    checkpoint_path = to_absolute_path(cfg.paths.checkpoint_path)

    # Create a PyTorch Lightning trainer with the generation callback
    root_dir = os.path.join(checkpoint_path, 'GraphLevel' + cfg.model_name)
    os.makedirs(root_dir, exist_ok=True)

    logger = None
    if cfg.mlflow.enabled:
        # MLFlow logger
        logger = MLFlowLogger(
            experiment_name=cfg.mlflow.experiment_name,
            run_name=f'{cfg.model_name}-{os.getpid()}',
            tracking_uri=cfg.mlflow.tracking_uri,
        )

        # Log all Hydra params with MLFlow
        for p_name, p_val in cfg.datamodule.items():
            logger.experiment.log_param(run_id=logger.run_id, key=p_name, value=str(p_val))

    # Initialize the trainer with the MLFlow logger and other trainer configs from the cfg
    trainer = Trainer(
        default_root_dir=root_dir,
        logger=logger,
        callbacks=[callbacks.ModelCheckpoint(dirpath=root_dir, save_weights_only=True, mode='max', monitor='val_acc')],
        **cfg.trainer,
    )

    # Load data
    datamodule = MUTAGDataModule(dataset_path, **cfg.datamodule)

    # Initialize model
    c_in = datamodule.num_node_features
    c_out = 1 if datamodule.num_classes == 2 else datamodule.num_classes
    model = GraphLevelGNN(**cfg.model, c_in=c_in, c_out=c_out)

    # Train the model
    trainer.fit(model, datamodule=datamodule)

    # Load the best model from the checkpoint
    best_model_path = trainer.checkpoint_callback.best_model_path
    model = GraphLevelGNN.load_from_checkpoint(best_model_path)

    # Test best model on validation and test set
    datamodule.setup(stage='test')
    test_result = trainer.test(model, datamodule=datamodule, verbose=False)
    test_accuracy = test_result[0]['test_acc']

    if cfg.mlflow.enabled:
        # Log the test results with MLFlow
        logger.experiment.log_metric(run_id=logger.run_id, key='test_accuracy', value=test_accuracy)


if __name__ == '__main__':
    train_graph_classifier()
